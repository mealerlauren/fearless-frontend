window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      const selectTag = document.getElementById('state');
      for (let state of data.states) {
        const option = document.createElement('option')
        option.value = state.abbreviation
        option.innerHTML = state.name
        selectTag.appendChild(option)
      }
    } else {
        console.error('error')
    }
})

window.addEventListener('DOMContentLoaded', async () => {
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
        const formData = new FormData(formTag);
        const formObject = Object.fromEntries(formData);
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formObject),
            headers: {
                'Content-Type': 'application/json',
            },
           };
        const locationUrl = "http://localhost:8000/api/locations/"
        const locationResponse = await fetch(locationUrl, fetchConfig);
            if (locationResponse.ok) {
                const data = await locationResponse.json();
                formTag.reset();

            } else {
                console.error("error")
        }
    }
    )
}
)
