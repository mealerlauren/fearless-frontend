function createCard(name, location, description, pictureUrl, startf, endf) {
  return `

      <div class="card m-3 shadow">
          <img src="${pictureUrl}" class="card-img-top">
          <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer">
            ${startf} - ${endf}
          </div>
          </div>
      </div>

  `;
}



window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw new Error("Response not okay");
    } else {
      const data = await response.json();
      let idx = 0
      for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const location = details.conference.location.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts).toISOString();
            const startf = `${startDate.slice(5,7)}/${startDate.slice(8,10)}/${startDate.slice(0,4)}`;
            const endDate = new Date(details.conference.ends).toISOString();
            const endf = `${endDate.slice(5,7)}/${endDate.slice(8,10)}/${endDate.slice(0,4)}`;
            const html = createCard(name, location, description, pictureUrl, startf, endf);
            const column = document.querySelectorAll('.col');
            column[idx % 3].innerHTML += html
          }
          idx ++
        }

      }
  } catch (error) {
    console.error("Error:", error);
  }
});
