window.addEventListener("DOMContentLoaded", async () => {
    const url = "http://localhost:8000/api/locations/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      const selectTag = document.getElementById("location");
      for (let location of data.locations) {
        const option = document.createElement("option");
        option.value = location.id;
        option.innerHTML = location.name;
        selectTag.appendChild(option);
      }
    } else {
      console.error("Error fetching locations.");
    }
  });

  window.addEventListener("DOMContentLoaded", async () => {
    const formTag = document.getElementById("create-conference-form");
    formTag.addEventListener("submit", async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const formObject = Object.fromEntries(formData);
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(formObject),
        headers: {
          "Content-Type": "application/json"
        }
      };
      const conferenceUrl = "http://localhost:8000/api/conferences/";
      const conferenceResponse = await fetch(conferenceUrl, fetchConfig);
      if (conferenceResponse.ok) {
        const data = await conferenceResponse.json();
        formTag.reset();
      } else {
        console.error("Error submitting conference data.");
      }
    });
  });
